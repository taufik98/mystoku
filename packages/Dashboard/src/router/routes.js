import DashboardLayout from '../layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import Dashboard from '@/views/Home.vue'
import Menu from '@/views/Dashboard/Menu.vue'
import Bahan from '@/views/Dashboard/Bahan.vue'
import Kategori from '@/views/Dashboard/Kategori.vue'
import Resep from '@/views/Dashboard/Resep.vue'
import BahanMasuk from '@/views/Dashboard/Bahan-Masuk.vue'
import BahanKeluar from '@/views/Dashboard/Bahan-Keluar.vue'
import Manag_Kasir from '@/views/Dashboard/Manag_Kasir.vue'
import Lap_Penjualan from '@/views/Dashboard/Lap-Penjualan.vue'
import Lap_Pendapatan from '@/views/Dashboard/Lap-Pendapatan.vue'
import Setting from '@/views/Dashboard/Setting.vue'
import Metode_Bayar from '@/views/Dashboard/Metode-Bayar.vue'
import KebijakanPrivasi from '@/views/KebijakanPrivasi.vue'

import Signin from '@/views/Auth/Signin.vue'
import Register from '@/views/Auth/Register.vue'
import NextRegister from '@/views/Auth/NextRegister.vue'
import Reset from '@/views/Auth/Reset.vue'
import Verifikasi from '@/views/Auth/Verifikasi.vue'
import AturPassword from '@/views/Auth/AturPassword.vue'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/signin'
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/lupa-password',
    name: 'Reset',
    component: Reset
  },
  {
    path: '/verifikasi',
    name: 'Verifikasi',
    component: Verifikasi
  },
  {
    path: '/atur-password',
    name: 'AturPassword',
    component: AturPassword
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/kebijakan-privasi',
    name: 'KebijakanPrivasi',
    component: KebijakanPrivasi
  },
  {
    path: '/tahap-register',
    name: 'NextRegister',
    component: NextRegister
  },
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/menu',
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
      {
        path: 'menu',
        name: 'Menu',
        component: Menu
      },
      {
        path: 'kategori',
        name: 'Kategori',
        component: Kategori
      },
      {
        path: 'bahan',
        name: 'Bahan',
        component: Bahan
      },
      {
        path: 'resep',
        name: 'Resep',
        component: Resep
      },
      {
        path: 'bahan-masuk',
        name: 'BahanMasuk',
        component: BahanMasuk
      },
      {
        path: 'bahan-keluar',
        name: 'BahanKeluar',
        component: BahanKeluar
      },
      {
        path: 'manajemen-karyawan',
        name: 'Manag_Kasir',
        component: Manag_Kasir
      },
      {
        path: 'laporan-penjualan',
        name: 'Lap_Penjualan',
        component: Lap_Penjualan
      },
      {
        path: 'laporan-pendapatan',
        name: 'Lap_Pendapatan',
        component: Lap_Pendapatan
      },
      {
        path: 'setting',
        name: 'Setting',
        component: Setting
      },
      {
        path: 'metode-pembayaran',
        name: 'Metode_Bayar',
        component: Metode_Bayar
      },
    ]
  },
  { path: '*', component: NotFound }
]

export default routes
