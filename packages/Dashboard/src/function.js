import router from './router/routes'
import moment from "moment";

export function debounce(fn, delay) {
  var timeoutID = null;
  return function() {
    clearTimeout(timeoutID);
    var args = arguments;
    var that = this;
    timeoutID = setTimeout(function() {
      fn.apply(that, args);
    }, delay);
  };
}

export async function CEKTOKEN() {
  let data = await localStorage.getItem('ID-Blast');

  if (data !== null) { // (!) jika data tidak sama dengan 0
      var newData = JSON.parse(data)
      var expired = newData.expired
      var dateNow = moment()
      var dateExpired = moment(expired, 'X');
      
      if (moment(dateNow).isAfter(dateExpired)) {
          localStorage.clear()
          router.push({ path: '/signin' });
          // console.log('Token dihapus karna expire! (-_-!) ');
      } else {
          // console.log('Token masih belum expire... (^_^)');
      }
  }
}

export function convertToRupiah(angka) {
  if (angka || angka === 0) {
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
      return 'Rp ' + rupiah.split('', rupiah.length - 1).reverse().join('');
  } else {
      return 'Rp 0';
  }
}
// Hasil dari angka 10000 -> Rp 10.000
export function convertToRibuan(angka) {
  if (angka) {
      var rupiah = '';
      var angkarev = angka.toString().split('').reverse().join('');
      for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
      return '' + rupiah.split('', rupiah.length - 1).reverse().join('');
  } else {
      return '0';
  }
}

export function validasiEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
