import axios from 'axios';
import { API_SERVER } from "@/config";

export function callapi() {
    const Token = localStorage.getItem('ID-Token') ? JSON.parse(localStorage.getItem('ID-Token')) : '';

    return new Promise((resolve, reject) => {
        resolve(axios.create({
            baseURL: API_SERVER,
            headers: {
                "Token" : Token,
                "Content-Type": "application/json",
            },
            crossdomain: true
        }))
    })
}