// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'

import BootstrapVue from 'bootstrap-vue'
import LightBootstrap from './light-bootstrap-main'
import { CEKTOKEN } from "@/function";
import routes from './router/routes'
import './registerServiceWorker'

Vue.use(LightBootstrap)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

Vue.config.productionTip = false

const router = new VueRouter({
  routes,
  mode: 'history',
  linkActiveClass: 'nav-item active',
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  // console.log(to.matched[0].path)
  if (to.matched.some(record => record.meta.requiresAuth)) {
    let token = localStorage.getItem('ID-Token');
    if (!token) {
      localStorage.clear()
      next('/signin')
    } else {
      next()
      CEKTOKEN()
    }
  } else {
    next()
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
