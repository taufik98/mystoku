// import DashboardLayout from "../layout/DashboardLayout.vue";
import DapurLayout from "../layout/Dapur/DapurLayout.vue";
// GeneralViews
import NotFound from "../components/GeneralViews/NotFoundPage.vue";

// Dapur pages
import Dapur from "@/views/Dapur/Dapur.vue";

import Kode from "@/views/Kode/Kode.vue";

const routes = [
  {
    path: "/",
    redirect: "/kode"
  },
  {
    path: "/kode",
    name: "kode",
    component: Kode
  },
  {
    path: "/dapur",
    component: DapurLayout,
    name: "Dapur",
    redirect: "/dapur/order",
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: "order",
        name: "Order",
        component: Dapur
      }
    ]
  },
  { path: "*", component: NotFound }
];

export default routes;
