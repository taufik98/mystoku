import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routes from './router/routes'

import "bootstrap/dist/css/bootstrap.css";
import BootstrapVue from 'bootstrap-vue'
import "font-awesome/css/font-awesome.min.css";
import 'bootstrap-vue/dist/bootstrap-vue.css'

import LightBootstrap from './light-bootstrap-main';
import './assets/css/mystyle.css';

Vue.config.productionTip = false

Vue.use(LightBootstrap)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

const router = new VueRouter({
  routes, // short for routes: routes
  mode: 'history',
  linkActiveClass: 'nav-item active',
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
})

new Vue({
  render: h => h(App),
  router,
  components: { App }
}).$mount('#app')
